# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 18:33:48 2022

@author: Byron Guamán
"""
#Importanción de librerias
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense
import pandas as pd
from matplotlib import pyplot
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.optimizers import Adam

#Lectura de información
admissions = pd.read_csv('dataset.csv')
datos = admissions.values
entrada = datos[:,0:12] 
salida = datos[:,-1] 

 # Escalado de datos
scaler = StandardScaler()
entrada_scaled= scaler.fit_transform(entrada)
  

# cargamos los datos (variables predictoras)
training_data = np.array(entrada_scaled, "float32")
target_data = np.array(salida, "float32")



#establecemos el modelo 
model = Sequential()
learn_rate=0.01
neurons=15
epocas=20
model.add(Dense(neurons, activation='relu', input_dim=12))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy',
            optimizer=Adam(learn_rate),
            metrics=['accuracy'])


#Probamos el modelo 
history=model.fit(training_data, target_data,epochs=epocas,batch_size=16)
_, train_acc = model.evaluate(training_data, target_data, verbose=1)

# Accuarcy
print('Accuracy')
print('Train: %.3f' % (train_acc*100))


#Métricas
# full metrics 
y_pred = model.predict(training_data).round()
y_true = target_data
# Matriz de confusión:
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_true, y_pred)
print(cm)
import matplotlib.pyplot as plt
import seaborn as sns
plt.figure(figsize = (8,4))
sns.heatmap(cm, annot=True, fmt='d')
plt.xlabel('Prediction', fontsize = 12)
plt.ylabel('Real', fontsize = 12)
plt.show()

# Exactitud:
from sklearn.metrics import accuracy_score
acc = accuracy_score(y_true, y_pred)
print('acc:', acc)

# Sensibilidad:
from sklearn.metrics import recall_score
rec = recall_score(y_true, y_pred)
print('recall:', rec)

# Precisión:
from sklearn.metrics import precision_score
prec = precision_score(y_true, y_pred)
print('precision:', prec)

# Puntuación F1:
from sklearn.metrics import f1_score
f1 = f1_score(y_true, y_pred)
print('F1:', f1)

# Área bajo la curva:
from sklearn.metrics import roc_auc_score
auc = roc_auc_score(y_true, y_pred)
print('AUC:', auc)

# R Score:(R^2 coefficient of determination)
from sklearn.metrics import r2_score
r2 = r2_score(y_true, y_pred)

print('R2:', r2)

#Gráficas 
pyplot.figure()
pyplot.title('Loss')
pyplot.plot(history.history['loss'], label='train')
pyplot.legend()
pyplot.figure()
pyplot.title('Accuracy')
pyplot.plot(history.history['accuracy'], label='train')
pyplot.legend()
pyplot.show()

model.save("mlp.h5")