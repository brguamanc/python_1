#Importanción de librerias

import pandas as pd
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import seaborn as sns
from tensorflow.keras.optimizers import Adam
from matplotlib import pyplot
import pickle as pk

#Lectura de información
datost = pd.read_csv('dataset.csv')
info=datost.values
datos = datost.iloc[:,0:12]
salida = info[:,-1]

ss = StandardScaler()
datos = ss.fit_transform(datos)
entrada = datos[:]
datos = pd.DataFrame(datos, columns = ['age','anaemia','creatinine_phosphokinase','diabetes','ejection_fraction','high_blood_pressure','platelets','serum_creatinine','serum_sodium','sex','smoking','time'])

#Se crea in modelo PCA de 6 Componentes
pca = PCA(n_components=6)
pca_values = pca.fit_transform(datos)

print(pca_values)

pk.dump(pca, open("pca_model.pkl", "wb"))

proyecciones = pd.DataFrame({'PCA1':pca_values[:,0], 'PCA2': pca_values[:,1], 'PCA3': 
                             pca_values[:,2], 'PCA4': pca_values[:,3], 'PCA5': pca_values[:,4], 'PCA6': 
                                 pca_values[:,5],'DEATH_EVENT':datost['DEATH_EVENT']})
print(proyecciones.head())

print(pca.explained_variance_ratio_)
print(pca.explained_variance_ratio_.sum())

sns.barplot(x=['PC1', 'PC2', 'PC3', 'PC4', 'PC5', 'PC6'], y = pca.explained_variance_ratio_)
plt.show()


training_data = np.array(proyecciones.iloc[:,0:6], "float32")
target_data = np.array(proyecciones.iloc[:,-1], "float32")

#establecemos el modelo 
model = Sequential()
learn_rate=0.01
neurons=15
epocas=20
model.add(Dense(neurons, activation='relu', input_dim=6))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy',
            optimizer=Adam(learn_rate),
            metrics=['accuracy'])


#Probamos el modelo 
history=model.fit(training_data, target_data,epochs=epocas,batch_size=16)
_, train_acc = model.evaluate(training_data, target_data, verbose=1)

# Accuarcy
print('Accuracy')
print('Train: %.3f' % (train_acc*100))


#Métricas
# full metrics 
y_pred = model.predict(training_data).round()
y_true = target_data
# Matriz de confusión:
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_true, y_pred)
print(cm)
import matplotlib.pyplot as plt
import seaborn as sns
plt.figure(figsize = (8,4))
sns.heatmap(cm, annot=True, fmt='d')
plt.xlabel('Prediction', fontsize = 12)
plt.ylabel('Real', fontsize = 12)
plt.show()

# Exactitud:
from sklearn.metrics import accuracy_score
acc = accuracy_score(y_true, y_pred)
print('acc:', acc)

# Sensibilidad:
from sklearn.metrics import recall_score
rec = recall_score(y_true, y_pred)
print('recall:', rec)

# Precisión:
from sklearn.metrics import precision_score
prec = precision_score(y_true, y_pred)
print('precision:', prec)

# Puntuación F1:
from sklearn.metrics import f1_score
f1 = f1_score(y_true, y_pred)
print('F1:', f1)

# Área bajo la curva:
from sklearn.metrics import roc_auc_score
auc = roc_auc_score(y_true, y_pred)
print('AUC:', auc)

# R Score:(R^2 coefficient of determination)
from sklearn.metrics import r2_score
r2 = r2_score(y_true, y_pred)

print('R2:', r2)

#Gráficas 
pyplot.figure()
pyplot.title('Loss')
pyplot.plot(history.history['loss'], label='train')
pyplot.legend()
pyplot.figure()
pyplot.title('Accuracy')
pyplot.plot(history.history['accuracy'], label='train')
pyplot.legend()
pyplot.show()

