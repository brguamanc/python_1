# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 17:50:34 2022

@author: Byron Guamán
"""
#Importanción de librerias
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense
import pandas as pd
from sklearn.preprocessing import StandardScaler

#Lectura de información
admissions = pd.read_csv('dataset.csv')
datos = admissions.values
entrada = datos[:,0:12] 
salida = datos[:,-1] 

 # Escalado de datos
scaler = StandardScaler()
entrada_scaled= scaler.fit_transform(entrada)
  

# cargamos los datos (variables predictoras)
training_data = np.array(entrada_scaled, "float32")
target_data = np.array(salida, "float32")


def create_model(neurons, learn_rate):
    model = Sequential()
    model.add(Dense(neurons, activation='relu', input_dim=12))
    model.add(Dense(1, activation='sigmoid'))
    opt = Adam(learning_rate=learn_rate)
    
    model.compile(loss='binary_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])
    return model

from tensorflow.keras.optimizers import Adam
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasClassifier
optimizers = ('adam') 
epochs = [5,20]
batches = [8, 16]
lr=[0.01, 0.001]
neurons = [10,15]

parameters = dict(nb_epoch=epochs, batch_size=batches, learn_rate=lr, neurons=neurons)
model = KerasClassifier(build_fn=create_model)
clf = GridSearchCV(estimator=model, param_grid=parameters, scoring='accuracy', n_jobs=1, cv=5)
clf = clf.fit(training_data, target_data)
print("\nbest parameters:")
print("Acc: ", clf.best_score_) 
print(clf.best_params_)